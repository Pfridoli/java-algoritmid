import java.util.*;

/**
 * This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

    private long num; //numerator
    private long den; //denominator

    /**
     * Main method. Different tests.
     */
    public static void main(String[] param) {
        Lfraction test1 = new Lfraction(5, 10);
        Lfraction test2 = new Lfraction(7, 10);

        System.out.println("fraction 1 : " + test1);
        System.out.println("fraction 2: " + test2);
        System.out.println("getnumerator: " + test1.getNumerator());
        System.out.println("getdenominator: " + test1.getDenominator());
        System.out.println("reduction: " + test1.reduction());
        System.out.println("tostring: " + test1.toString());
        System.out.println("hashcode: " + test1.hashCode());
        System.out.println("plus: " + test1.plus(test2));
        System.out.println("times: " + test1.times(test2));
        System.out.println("inverse: " + test1.inverse());
        System.out.println("opposite: " + test1.opposite());
        System.out.println("minus: " + test1.minus(test2));
        System.out.println("divideby: " + test1.divideBy(test2));
        try {
            System.out.println("clone: " + test1.clone());
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        System.out.println("integerpart: " + test1.integerPart());
        System.out.println("fractionpart: " + test1.fractionPart());
        System.out.println("todouble: " + test1.toDouble());
        System.out.println("tolfraction: " + toLfraction(5, 5));
        System.out.println("valueof: " + valueOf("25/-50"));
    }

    /**
     * Constructor.
     *
     * @param a numerator
     * @param b denominator > 0
     */
    public Lfraction(long a, long b) {
        if (b == 0) {
            throw new RuntimeException("Denominator can't be 0");
        }
        this.num = a;
        this.den = b;
    }

    /**
     * Public method to access the numerator field.
     *
     * @return numerator
     */
    public long getNumerator() {
        return this.num;
    }

    /**
     * Public method to access the denominator field.
     *
     * @return denominator
     */
    public long getDenominator() {
        return this.den;
    }

    /**
     * Conversion to string.
     *
     * @return string representation of the fraction
     */
    @Override
    public String toString() {
        return Long.toString(this.num) + "/" + Long.toString(this.den);
    }

    /**
     * Equality test.
     *
     * @param m second fraction
     * @return true if fractions this and m are equal
     */
    @Override
    public boolean equals(Object m) {
        Lfraction other = (Lfraction) m;
        if (this.den == other.den) {
            return this.num == other.num;
        } else {
            long dif; //1. mistake
            if (other.den > this.den) {
                dif = other.den / this.den;
            } else {
                dif = this.den / other.den;
            }
            if ((this.num * dif) == other.num) {
                return true;
            }
        }
        return false;
    }

    /**
     * Hashcode has to be equal for equal fractions.
     * Idea from https://docs.oracle.com/javase/7/docs/api/java/util/Objects.html
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.num, this.den);
    }

    /**
     * Sum of fractions.
     *
     * @param m second addend
     * @return this+m
     */
    public Lfraction plus(Lfraction m) {
        if (this.den == m.den) {
            return new Lfraction(this.num + m.num, this.den).reduction();
        } else {
            return new Lfraction((m.num * this.den) + (this.num * m.den), (m.den * this.den)).reduction();
        }
    }

    /**
     * Multiplication of fractions.
     *
     * @param m second factor
     * @return this*m
     */
    public Lfraction times(Lfraction m) {
        return new Lfraction(this.num * m.num, this.den * m.den).reduction();
    }

    /**
     * Inverse of the fraction. n/d becomes d/n.
     *
     * @return inverse of this fraction: 1/this
     */
    public Lfraction inverse() {
        if (this.den == 0) {
            throw new RuntimeException("Denominator can't be 0 (inverse error)");
        }
        if (num < 1) {
            return new Lfraction(-this.den, -this.num).reduction();
        } else {
            return new Lfraction(this.den, this.num).reduction();
        }
    }

    /**
     * Opposite of the fraction. n/d becomes -n/d.
     *
     * @return opposite of this fraction: -this
     */
    public Lfraction opposite() {
        return new Lfraction(-this.num, this.den);
    }

    /**
     * Difference of fractions.
     *
     * @param m subtrahend
     * @return this-m
     */
    public Lfraction minus(Lfraction m) {
        if (this.den == m.den) {
            return new Lfraction(this.num - m.num, this.den);
        } else {
            return new Lfraction((this.num * m.den) - (m.num * this.den), (m.den * this.den));
        }
    }

    /**
     * Quotient of fractions.
     *
     * @param m divisor
     * @return this/m
     */
    public Lfraction divideBy(Lfraction m) {
        if (this.den == 0 || m.den == 0) {
            throw new RuntimeException("Denominator can't be 0 (divideBy error)");
        }
        return times(m.inverse());
    }

    /**
     * Comparision of fractions.
     *
     * @param m second fraction
     * @return -1 if this < m; 0 if this==m; 1 if this > m
     */
    @Override
    public int compareTo(Lfraction m) {
        if (this.equals(m)) {
            return 0;
        } else if (this.num * m.den < m.num * this.den) {
            return -1;
        } else {
            return 1;
        }
    }

    /**
     * Clone of the fraction.
     *
     * @return new fraction equal to this
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Lfraction(this.num, this.den);
    }

    /**
     * Integer part of the (improper) fraction.
     *
     * @return integer part of this fraction
     */
    public long integerPart() {
        return this.num / this.den;
    }

    /**
     * Extract fraction part of the (improper) fraction
     * (a proper fraction without the integer part).
     *
     * @return fraction part of this fraction
     */
    public Lfraction fractionPart() {
        if (this.num >= this.den || -this.num >= this.den) {
            return new Lfraction(this.num % this.den, this.den);
        } else {
            return new Lfraction(this.num, this.den);
        }
    }

    /**
     * Approximate value of the fraction.
     *
     * @return numeric value of this fraction
     */
    public double toDouble() {
        return (double) this.num / this.den;
    }

    /**
     * Double value f presented as a fraction with denominator d > 0.
     *
     * @param f real number
     * @param d positive denominator for the result
     * @return f as an approximate fraction of form n/d
     */
    public static Lfraction toLfraction(double f, long d) {
        if (d == 0) {
            throw new RuntimeException("Denominator can't be 0 (toLfraction error)");
        }
        long number = Math.round(f * d);
        return new Lfraction(number, d);
    }

    /**
     * Conversion from string to the fraction. Accepts strings of form
     * that is defined by the toString method.
     *
     * @param s string form (as produced by toString) of the fraction
     * @return fraction represented by s
     */
    public static Lfraction valueOf(String s) {
        if (s.trim().isEmpty()) {
            throw new IndexOutOfBoundsException("valueOf method failed, because String was empty");
        }
        s = s.replaceAll("\\s", "");

        if (!s.matches("[\\d\\s*.+/-]+")) {
            throw new RuntimeException("Illegal input error in String s: " + s);
        }
        if (!s.contains("/")) {
            throw new RuntimeException("String must contain character / : " + s);
        }
        String[] parts = s.split("/");

        if (parts.length != 2) {
            throw new IllegalArgumentException("String size is wrong. Must contain 2 separate numbers and 1 symbol /: " + s);
        }
        long numeratorS = 0;
        long denumatorS = 0;

        try {
            numeratorS = Long.parseLong(parts[0]);
            denumatorS = Long.parseLong(parts[1]);
        } catch (NumberFormatException e) {
            System.out.println("parseLong error: " + e);
        }

        return new Lfraction(numeratorS, denumatorS).reduction();
    }

    /**
     * Method that finds the greatest common divisor of two integers
     * to reduce the fractions
     * Idea from http://www.guideforschool.com/2469030-java-program-to-find-the-gcd-of-two-numbers-division-method/
     *
     * @return reduced Lfraction
     */
    public Lfraction reduction() {
        long common;
        if (this.num == this.den) {
            common = this.num;
        } else {
            long n1;
            long n2;
            long mem;
            if (this.num > this.den) {
                n1 = this.num;
                n2 = this.den;
            } else {
                n1 = this.den;
                n2 = this.num;
            }
            while (n2 != 0) {
                mem = n1 % n2;
                n1 = n2;
                n2 = mem;
            }
            common = n1;
        }
        long answerNum = this.num / common;
        long answerDen = this.den / common;

        if (answerDen < 0) {
            answerNum = -answerNum;
            answerDen = -answerDen;
        }

        return new Lfraction(answerNum, answerDen);
    }
}