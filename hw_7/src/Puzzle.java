import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Program, that tries to solve cryptarithmetic puzzle.
 * <p>
 * IDEAS TAKEN FROM:
 * 1. http://enos.itcollege.ee/~jpoial/algoritmid/tehnikad.html (exercise examples)
 * 2. http://www.geeksforgeeks.org/backtracking-set-8-solving-cryptarithmetic-puzzles/
 * 3. Friends helped some parts too (answerChecker, cryptarithmetic mainly)
 */
public class Puzzle {

    private long charValue;
    private char character;

    /**
     * Puzzle object constructor.
     *
     * @param charValue value of character.
     * @param character character.
     */
    private Puzzle(long charValue, char character) {
        this.charValue = charValue;
        this.character = character;
    }

    /**
     * Sets new value to certain character
     *
     * @param charValue characters value
     */
    private void setCharValue(long charValue) {
        this.charValue = charValue;
    }

    /**
     * Get method for charValue
     *
     * @return charValue
     */
    private long getCharValue() {
        return charValue;
    }

    /**
     * Get method for character
     *
     * @return returns current character.
     */
    private char getCharacter() {
        return character;
    }

    /**
     * Method to add 1 to charValue.
     *
     * @param charValue current charValue
     */
    private void increase(long charValue) {
        this.charValue = charValue + 1;
    }

    /**
     * Solve the cryptarithmetic puzzle.
     *
     * @param args three words for puzzle
     */
    public static void main(String[] args) {
        //cryptarithmetic(args[0], args[1], args[2]);
        //cryptarithmetic("Send", "More", "Money");
        //cryptarithmetic("YKS", "KAKS", "KOLM");
        cryptarithmetic("ABCDEFGHIJAB", "ABCDEFGHIJA", "ACEHJBDFGIAC"); //TAKES LONG TIME!!!
    }

    /**
     * Method, that solves the cryptarithmetic puzzle.
     *
     * @param first  first word.
     * @param second second word.
     * @param sum    final word.
     */
    private static void cryptarithmetic(String first, String second, String sum) {
        String checkedStrings[] = conditionChecker(first, second, sum);
        char firstChar[] = checkedStrings[0].toCharArray();
        char secondChar[] = checkedStrings[1].toCharArray();
        char sumChar[] = checkedStrings[2].toCharArray();
        ArrayList<Puzzle> characterArrayList = makeArrayList(checkedStrings[0], checkedStrings[1], checkedStrings[2]);
        int numberOfSolutions = 0;
        int n = characterArrayList.size() - 1;
        while (n >= 0) {
            if (characterArrayList.get(n).getCharValue() <= 9) {
                if (checkArray(characterArrayList)) {
                    if (answerChecker(characterArrayList, checkedStrings[0], checkedStrings[1], checkedStrings[2],
                            firstChar[0], secondChar[0], sumChar[0])) {
                        numberOfSolutions++;
                        printSolution(characterArrayList, numberOfSolutions);
                    }
                }
            } else {
                while (characterArrayList.get(n).getCharValue() >= 9) {
                    characterArrayList.get(n).setCharValue(0);
                    n--;
                    if (n < 0) {
                        break;
                    }
                }
                if (n >= 0) {
                    characterArrayList.get(n).increase(characterArrayList.get(n).getCharValue());
                    n = characterArrayList.size() - 1;
                    characterArrayList.get(n).setCharValue(-1);
                }
            }
            if (n > 0) {
                characterArrayList.get(n).increase(characterArrayList.get(n).getCharValue());
            }
        }
        printNumberSolutions(numberOfSolutions);
    }

    /**
     * Method, that checks the strings, if there are illegal symbols or the size is wrong. Also makes strings uppercase
     *
     * @param first  first word
     * @param second second word
     * @param sum    final word
     * @return Returns checked strings
     */
    private static String[] conditionChecker(String first, String second, String sum) {
        if (first.length() > 18)
            throw new RuntimeException("First word is too long (18 is maximum): " + first);
        if (second.length() > 18)
            throw new RuntimeException("Second word is too long (18 is maximum): " + second);
        if (sum.length() > 18)
            throw new RuntimeException("Final word is too long (18 is maximum): " + sum);

        first = first.toUpperCase();
        second = second.toUpperCase();
        sum = sum.toUpperCase();

        first = first.replaceAll("\\s+", "");
        second = second.replaceAll("\\s+", "");
        sum = sum.replaceAll("\\s+", "");

        if (!first.matches("^[A-Z]+$"))
            throw new RuntimeException("First word contains illegal letters: " + first);
        if (!second.matches("^[A-Z]+$"))
            throw new RuntimeException("Second word contains illegal letters: " + second);
        if (!sum.matches("^[A-Z]+$"))
            throw new RuntimeException("Final word contains illegal letters: " + sum);

        return new String[]{first, second, sum};
    }

    /**
     * Method, that makes arrayList which contains characters
     *
     * @param first  First word
     * @param second Second word
     * @param sum    Final word
     * @return ArrayList of characters
     */
    private static ArrayList<Puzzle> makeArrayList(String first, String second, String sum) {
        ArrayList<Puzzle> arrayList = new ArrayList<>();
        Map<Character, Void> checkMap = new HashMap<>();
        Puzzle character;

        for (char c : first.toCharArray()) {
            if (!checkMap.containsKey(c)) {
                character = new Puzzle(0, c);
                checkMap.put(c, null);
                arrayList.add(character);
            }
        }
        for (char c : second.toCharArray()) {
            if (!checkMap.containsKey(c)) {
                character = new Puzzle(0, c);
                checkMap.put(c, null);
                arrayList.add(character);
            }
        }

        for (char c : sum.toCharArray()) {
            if (!checkMap.containsKey(c)) {
                character = new Puzzle(0, c);
                checkMap.put(c, null);
                arrayList.add(character);
            }
        }

        if (arrayList.size() >= 11)
            throw new RuntimeException("There are too many different kind of characters (10 is maximum): "
                    + arrayList.size());

        return arrayList;
    }

    /**
     * Method, that checks array if every character value is different.
     *
     * @param characters ArrayList of characters.
     * @return true when character have different value, false when not
     */
    private static boolean checkArray(ArrayList<Puzzle> characters) {
        long[] mLong = new long[10];

        for (Puzzle aNcharaters : characters) {
            int index = (int) aNcharaters.getCharValue();
            mLong[index]++;
            if (mLong[index] > 1) {
                return false;
            }
        }
        return true;
    }

    /**
     * Method, that checks first and seconds words are equal to final word
     *
     * @param characters ArrayList of puzzle elements.
     * @param first      First word.
     * @param second     Second word.
     * @param sum        Final word.
     * @param firstChar  First word, first character.
     * @param secondChar Second word, first character.
     * @param sumChar    Final word, first character.
     * @return Returns true when first and second words are equal to final word, false when not
     */
    private static boolean answerChecker(ArrayList<Puzzle> characters, String first, String second, String sum, char firstChar, char secondChar, char sumChar) {
        long firstWordValue = 0;
        long secondWordValue = 0;
        long thirdWordValue = 0;
        int totalWordLength = (first + second + sum).length();
        char stringTogether[] = (first + second + sum).toCharArray();
        long[] totalLength = new long[totalWordLength];

        for (int j = 0; j < totalWordLength; j++) {
            for (Puzzle charactersA : characters) {
                char charaGet = charactersA.getCharacter();
                long charaValue = charactersA.getCharValue();
                if (charaGet == firstChar || charaGet == secondChar || charaGet == sumChar) {
                    if (charaValue == 0) {
                        return false;
                    }
                }
                if (charaGet == stringTogether[j]) {
                    totalLength[j] = charaValue;
                    if (j < first.length()) {
                        firstWordValue += totalLength[j] * Math.pow(10, (first.length() - j - 1));
                    } else if (j >= first.length() && j < (second.length() + first.length())) {
                        secondWordValue += totalLength[j] * Math.pow(10, (second.length() - j + (first.length() - 1)));
                    } else {
                        thirdWordValue += totalLength[j] * Math.pow(10, (sum.length() - j + (second.length() + first.length() - 1)));
                    }
                }
            }
        }
        return firstWordValue + secondWordValue == thirdWordValue;
    }

    /**
     * Method, that prints out current solution to cryptarithmetic puzzle
     *
     * @param characterArrayList contains founded solution to the puzzle
     * @param numberOfSolutions  current founded solution number
     */
    private static void printSolution(ArrayList<Puzzle> characterArrayList, int numberOfSolutions) {
        System.out.println(numberOfSolutions + ". solution is: ");
        for (Puzzle characterArrayListAn : characterArrayList) {
            System.out.println(characterArrayListAn.getCharacter() + "-" + characterArrayListAn.getCharValue());
        }
        System.out.println();
    }

    /**
     * Method, that prints out number of solutions to cryptarithmetic puzzle
     *
     * @param numberOfSolutions number of solutions to the problem
     */
    private static void printNumberSolutions(int numberOfSolutions) {
        if (numberOfSolutions == 0) {
            System.out.println("There are no solutions to this puzzle");
        } else if (numberOfSolutions >= 1) {
            System.out.println("There are " + numberOfSolutions + " solutions to this puzzle");
        }
    }
}