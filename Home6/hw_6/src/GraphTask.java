/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 * Homework 6 and exercise 14 - Produce a method that takes the shortest path
 * between two predetermined vertices in a given coherent simple graph.
 */
public class GraphTask {
    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     */
    public void run() {
        Graph g = new Graph("G");
        g.createRandomSimpleGraph(2450, 2500);
        //g.matrixMaker(g.createAdjMatrix());
        //WARNING, source and destination should be -1 lower than vertices, because it uses matrix
        g.dijkstra(g.createAdjMatrix(), 2450, 0, 3);

        //int feedback[];
        //feedback = questions();
        //g.createRandomSimpleGraph(feedback[0], feedback[1]);
        //g.matrixMaker(g.createAdjMatrix());
        //g.dijkstra(g.createAdjMatrix(),feedback[0], feedback[2], feedback[3]);

        //System.out.println(g);
    }

    /**
     * Method that ask user for number of Vertices and edges.
     * Also ask for starting position and destination.
     *
     * @return All user inputs for SimpleGraph
     */
    private int[] questions() {
        int answers[] = new int[4];
        while (true) {
            System.out.println("Enter number of vertices:");
            answers[0] = TextIO.getlnInt();
            if (answers[0] >= 0) {
                break;
            }
        }
        while (true) {
            System.out.println("Enter number of edges");
            answers[1] = TextIO.getlnInt();
            if (answers[1] >= 0) {
                break;
            }
        }
        while (true) {
            System.out.println("Enter starting position (source):");
            answers[2] = TextIO.getlnInt() - 1; //minus is for matrix table
            if (answers[2] > -1) {
                break;
            }
        }
        while (true) {
            System.out.println("Enter final destination (destination):");
            answers[3] = TextIO.getlnInt() - 1;  //minus is for matrix table
            if (answers[3] > -1) {
                break;
            }
        }
        return answers;
    }

    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }

    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private String id;
        private Vertex target;
        private Arc next;

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

    }

    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuilder sb = new StringBuilder(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param vertices number of vertices
         * @param edges    number of edge
         */
        public void createRandomSimpleGraph(int vertices, int edges) {
            if (vertices <= 0)
                throw new IllegalArgumentException("Vertices can't be zero or less");
            if (vertices > 2500)
                throw new IllegalArgumentException("Too many vertices: " + vertices);
            if (edges < vertices - 1 || edges > vertices * (vertices - 1) / 2)
                throw new IllegalArgumentException("Impossible number of edges: " + edges);

            first = null;
            createRandomTree(vertices);       // n-1 edges created here
            Vertex[] vert = new Vertex[vertices];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = edges - vertices + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * vertices);  // random source
                int j = (int) (Math.random() * vertices);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        /**
         * Prints out the whole matrix
         *
         * @param connected matrix, that contains all of the vertices and edges
         */
        private void matrixMaker(int connected[][]) {

            System.out.println("Vertex as matrix table:");
            for (int[] aConnected : connected) {
                for (int anAConnected : aConnected) {
                    System.out.print(anAConnected + " ");
                }
                System.out.println();
            }
            System.out.println();
        }

        /**
         * Function that implements Dijkstra's shortest path algorithm from given source to destination
         * for a graph represented using adjacency matrix representation
         * <p>
         * IDEAS TAKEN FROM: 1. http://www.geeksforgeeks.org/printing-paths-dijkstras-shortest-path-algorithm/
         * 2. http://www.geeksforgeeks.org/greedy-algorithms-set-6-dijkstras-shortest-path-algorithm/
         * 3. http://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
         *
         * @param connected   matrix, that contains all of the vertices and edges
         * @param source      vertex starting position
         * @param destination vertex final destination
         * @param vertices    number of vertices
         */
        private void dijkstra(int connected[][], int vertices, int source, int destination) {
            if (source < 0 || destination < 0)
                throw new IndexOutOfBoundsException("Source or Destination can't be smaller than zero: " +
                        (source + 1) + "/" + (destination + 1));
            if (source + 1 > vertices)
                throw new IndexOutOfBoundsException("Source can't be bigger than vertices: " +
                        (source + 1) + "/" + vertices);
            if (destination + 1 > vertices)
                throw new IndexOutOfBoundsException("Destination can't be bigger than vertices: " +
                        (destination + 1) + "/" + vertices);

            int dist[] = new int[vertices];
            String path[] = new String[vertices];
            Boolean sptSet[] = new Boolean[vertices];
            for (int i = 0; i < vertices; i++) { // Initialize all distances as INFINITE, sptSet[] as false and path
                dist[i] = Integer.MAX_VALUE;
                sptSet[i] = false;
                path[i] = "";
            }
            dist[source] = 0;
            path[source] += String.valueOf(source + 1);
            for (int count = 0; count < vertices - 1; count++) { // Find shortest path for all vertices
                int u = minDistance(dist, sptSet, vertices); // Pick the minimum distance vertex from the set of vertices not yet processed.
                if (u == destination) { //stops for cycle if finds it's destination
                    break;
                }
                sptSet[u] = true; // Mark the picked vertex as processed
                for (int f = 0; f < vertices; f++) { // Update dist value of the adjacent vertices of the picked vertex.
                    if (!sptSet[f] && connected[u][f] != 0 && dist[u] != Integer.MAX_VALUE &&
                            dist[u] + connected[u][f] < dist[f]) {
                        dist[f] = dist[u] + connected[u][f];
                        path[f] = (path[u]) + "-" + (f + 1);
                    }
                }
            }
            printSolution(dist, path, source, destination); //prints out shortest distance
        }


        /**
         * Method that finds the vertex with minimum distance value,
         * from the set of vertices not yet included in shortest path tree
         *
         * @param dist     array, that holds the shortest distance from source to destination
         * @param sptSet   boolean, that shows if vertex is included in shortest path tree
         * @param vertices number of vertices
         * @return Minimum distance value
         */
        private int minDistance(int[] dist, Boolean[] sptSet, int vertices) {
            int min = Integer.MAX_VALUE;
            int min_index = -1;
            for (int v = 0; v < vertices; v++) {
                if (!sptSet[v] && dist[v] <= min) {
                    min = dist[v];
                    min_index = v;
                }
            }
            return min_index;
        }

        /**
         * Method that prints out graph shortest route from source to destination (still uses matrix)
         *
         * @param dist        array, that holds the shortest distance from source to destination
         * @param path        array, that stores shortest path tree
         * @param source      vertex starting position
         * @param destination number of vertices
         */
        private void printSolution(int[] dist, String[] path, int source, int destination) {
            System.out.println("Shortest path from " + (source + 1) + " to " + (destination + 1) + " is:");
            System.out.println("Vertex Distance Path");
            System.out.println((source + 1) + "->" + (destination + 1) + "\t" +
                    dist[destination] + "\t\t" + path[destination]);
        }
    }
}