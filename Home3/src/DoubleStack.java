import java.util.LinkedList;

//Ideas taken from http://enos.itcollege.ee/~jpoial/algoritmid/adt.html
public class DoubleStack {

    private LinkedList<Double> test;

    public static void main(String[] argum) {
        double answer = interpret("4 x");
        System.out.println(answer); //Answer should be -13
    }

    // Constructor for a new stack
    DoubleStack() {
        this.test = new LinkedList<Double>();
    }

    //Copy of the stack
    @Override
    public Object clone() throws CloneNotSupportedException {
        DoubleStack clone = new DoubleStack();
        for (int i = 0; i < test.size(); i++) {
            clone.test.add(test.get(i));
        }
        return clone;
    }

    // Check whether the stack is empty
    public boolean stEmpty() {
        return test.isEmpty();
    }

    // Adding an element to the stack
    public void push(double a) {
        test.push(a);
    }

    // Removing an element from the stack
    public double pop() {
        if (stEmpty())
            throw new IndexOutOfBoundsException("Stack is empty");
        return test.pop();
    }

    //Arithmetic operation s between two topmost elements of the stack
    //(result is left on top)
    public void op(String s) {
        if (stEmpty())
            throw new IndexOutOfBoundsException("Stack is empty");
        double op2 = pop();

        if (s.isEmpty())
            throw new IndexOutOfBoundsException("String is empty");


        double op1 = pop();

        if (s.equals("+")) {
            push(op1 + op2);
        }
        if (s.equals("-")) {
            push(op1 - op2);
        }
        if (s.equals("*")) {
            push(op1 * op2);
        }
        if (s.equals("/")) {
            push(op1 / op2);
        }

    }

    //Reading the top without removing it
    public double tos() {
        if (stEmpty())
            throw new IndexOutOfBoundsException("Stack is empty");
        return test.element();
        //return test.getFirst(); basically the same thing
    }

    //Check whether two stacks are equal
    public boolean equals(Object o) {

        return test.equals(((DoubleStack) o).test);
    }

    //Conversion of the stack to string (top last)
    @Override
    public String toString() {
        StringBuffer stb = new StringBuffer();
        for (int i = test.size() - 1; i >= 0; i--) { //starts at the end
            stb.append(test.get(i) + ". ");
        }
        return stb.toString();

    }

    //Starts interpretation
    public static double interpret(String pol) {
        if (pol.trim().isEmpty())
            throw new IndexOutOfBoundsException("There is nothing to interpretent, string is empty");
        if (!pol.matches("[\\d\\s*.+/-]+"))
            throw new RuntimeException("Illegal input error in String pol. " + pol);

        DoubleStack ds = new DoubleStack();
        String[] parts = pol.trim().split("\\s+");
        int counter = 0;
        boolean status;
        if (parts.length == 1 || parts.length == 3) {
            for (int i = 0; i < parts.length; i++) {
                try {
                    Double.parseDouble(parts[i]); //Checks if the part is number or not
                    status = true;
                } catch (Exception e) {
                    if (i == 0 || i == 1) {
                        throw new RuntimeException("Polish notation not correct " + pol);
                    }
                    status = false;
                }
                if (status) {
                    ds.push(Double.parseDouble(parts[i])); //Makes the element into double and adds to the stack
                    counter++;
                } else {
                    ds.op(parts[i]); //Starts doing math
                }
            }
        }

        if (counter == (parts.length - counter) + 1) {
            return ds.tos();
        } else {
            throw new RuntimeException("Polish notation not correct " + pol);
        }
    }
}

