import java.util.*;

public class TreeNode {

    private String name;
    private TreeNode firstChild;
    private TreeNode nextSibling;

    /**
     * Constructor for TreeNode
     * @param n name
     * @param d firstChild
     * @param r nextSibling
     */
    TreeNode(String n, TreeNode d, TreeNode r) {
        name = n;
        firstChild = d;
        nextSibling = r;
    }

    /**
     * Starts tree node method
     * Ideas taken from http://interactivepython.org/runestone/static/pythonds/Trees/ParseTree.html
     * @param s String to start tree node process
     * @return treeNode Returns completed tree
     */
    public static TreeNode parsePrefix(String s) {
        if (s.trim().isEmpty())
            throw new RuntimeException("ParsePrefix error. String is empty: ");
        if (s.contains("(("))
            throw new RuntimeException("ParsePrefix error. Double bracelets detected: " + s);
        if (s.contains("()"))
            throw new RuntimeException("ParsePrefix error. Empty bracelet detected: " + s);
        if (s.contains(",,"))
            throw new RuntimeException("ParsePrefix error. Double commas detected: " + s);
        if (s.contains(" "))
            throw new RuntimeException("ParsePrefix error. Empty spaces detected: " + s);
        if (s.contains(",") && !(s.contains("(") && s.contains(")")))
            throw new RuntimeException("ParsePrefix error. Bracelet is not finished correctly: " + s);

        //ADDED ADDITIONAL EXCEPTIONS
        if (s.contains(",(") || s.contains("(,") || s.contains(",)"))
            throw new RuntimeException("ParsePrefix error. Comma and bracelet is positioned wrongly: " + s);
        if (!s.matches("[\\w(),+*/-]+") )
            throw new RuntimeException("ParsePrefix error. Illegal element founded in string: " + s);
        if (s.contains(")(") || s.contains("),("))
            throw new RuntimeException("ParsePrefix error. Element is missing between the bracelet: " + s);
        if (s.startsWith("("))
            throw new RuntimeException("ParsePrefix error. First character can't be root: " + s);

        Stack<TreeNode> treeNodeStack = new Stack<>();
        String[] parts = s.split("");
        TreeNode treeNode = new TreeNode(null, null, null);
        boolean root = false;

        for (int i = 0; i < parts.length; i++) {
            if (parts[i].equals("(")) {
                if (root)
                    throw new RuntimeException("ParsePrefix error. There can't be multiple elements in root: " + s);
                treeNodeStack.push(treeNode);
                treeNode.firstChild = new TreeNode(null, null, null);
                treeNode = treeNode.firstChild;
                if (parts[i + 1].trim().equals(","))
                    throw new RuntimeException("ParsePrefix error. Element can't be comma: " + s);
            } else if (parts[i].equals(",")) {
                if (root)
                    throw new RuntimeException("ParsePrefix error. There can't be multiple elements in root: " + s);
                treeNode.nextSibling = new TreeNode(null, null, null);
                treeNode = treeNode.nextSibling;
            } else if (parts[i].equals(")")) {
                treeNode = treeNodeStack.pop();
                if (treeNodeStack.isEmpty()) {
                    root = true;
                }
            } else {
                if (treeNode.name == null) {
                    treeNode.name = parts[i];
                } else {
                    if (parts[i-1].trim().equals(")"))
                        throw new RuntimeException("ParsePrefix error. Bracelet error: " + s);
                    treeNode.name += parts[i];
                }
            }
        }
        return treeNode;
    }

    /**
     * Method, that makes tree node into String. NB Starts from left.
     * @return b, tree that is in string form
     */
    public String rightParentheticRepresentation() {
        if (name.trim().isEmpty())
            throw new RuntimeException("RightParentheticRepresentation error. Tree node is empty");

        StringBuilder b = new StringBuilder();
        if (firstChild != null) {
            b.append("(").append(firstChild.rightParentheticRepresentation()).append(")");
        }
        if (nextSibling != null) {
            b.append(name).append(",").append(nextSibling.rightParentheticRepresentation());
        } else {
            b.append(name);
        }
        return b.toString();
    }

    /**
     * Main method
     * @param param
     */
    public static void main(String[] param) {
        String s1 = "A(B1,v(uuu),k,D)";
        TreeNode t1 = TreeNode.parsePrefix(s1);
        String v1 = t1.rightParentheticRepresentation();
        System.out.println(s1 + " ==> " + v1); // A(B1,v(uuu)k,D) ==> (B1,(uuu)vk,D)A

        String s2 = "B(D(C,H),B(W,M))";
        TreeNode t2 = TreeNode.parsePrefix(s2);
        String v2 = t2.rightParentheticRepresentation();
        System.out.println(s2 + " ==> " + v2); // B(D(C,H),B(W,M))) ==> ((C,H)D,(W,M)B)B

        String s3 = "A(B(D(G,H),E,F(I)),C(J))";
        TreeNode t3 = TreeNode.parsePrefix(s3);
        String v3 = t3.rightParentheticRepresentation();
        System.out.println(s3 + " ==> " + v3); // A(B(D(G,H),E,F(I)),C(J)) ==> (((G,H)D,E,(I)F)B,(J)C)A
    }
}