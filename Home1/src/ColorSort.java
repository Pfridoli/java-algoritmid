public class ColorSort {

    enum Color {red, green}


    public static void main(String[] param) {
        // for debugging
    }

    public static void reorder(Color[] balls) {
        int redCounter = 0;
        int greenCounter = 0;
        int ballCount = balls.length;

        // Counts how many coloured balls are there (red and green only)
        for (int i = 0; i < ballCount; i++) {
            if (balls[i] == Color.red) {
                redCounter++;
            } else if (balls[i] == Color.green) {
                greenCounter++;
            }
        }

        // If all the balls are red, green or blue then it's going to return
        if (redCounter == ballCount) {
            return;
        } else if (greenCounter == ballCount) {
            return;
        }


        // Makes the coloured balls array in correct order(red,green,blue)
        for (int i = 0; i < redCounter; i++) {
            balls[i] = Color.red;
        }
        for (int i = redCounter; i < redCounter + greenCounter; i++) {
            balls[i] = Color.green;
        }
    }
}
